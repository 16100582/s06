from abc import ABC, abstractmethod
class Request:
    def __init__(self, name, requester, dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = 'Open'

    def setStatus(self, status):
        valid_statuses = ["open", "closed", "cancelled"]
        if status.lower() not in valid_statuses:
            raise ValueError("Invalid status. Valid statuses are open, closed, and cancelled.")
        self.status = status.lower()
        return self.status

    def updateRequest(self, new_requester=None):
        if new_requester:
            self.requester = new_requester
        self.status = f'Request {self.name} has been updated'
        return self.status

    def closeRequest(self):
        self.status = f'Request {self.name} has been closed'
        return self.status

    def cancelRequest(self):
        self.status = f'Request {self.name} has been cancelled'
        return self.status

class Person(ABC):
    def __init__(self, name):
        self.name = name
        self.requests = []

    def getFullName(self):
        return self.name

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        for request in self.requests:
            print(request.name, request.requester, request.dateRequested, request.status)
        return "Request has been checked"    

    def addUser(self, user):
        return "User has been added"

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName + " " + lastName)
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department

    # Getter methods
    def getFirstName(self):
        return self.__firstName

    def getLastName(self):
        return self.__lastName

    def getEmail(self):
        return self.__email

    def getDepartment(self):
        return self.__department

    # Setter methods
    def setFirstName(self, firstName):
        self.__firstName = firstName

    def setLastName(self, lastName):
        self.__lastName = lastName

    def setEmail(self, email):
        self.__email = email

    def setDepartment(self, department):
        self.__department = department

    # Abstract methods
    def checkRequest(self):
        return "Request has been checked"

    def addUser(self):
        return "User has been added"

    # Public methods
    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"


class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName + " " + lastName)
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department
        self.__members = []

    # Getter methods
    def getFirstName(self):
        return self.__firstName

    def getLastName(self):
        return self.__lastName

    def getEmail(self):
        return self.__email

    def getDepartment(self):
        return self.__department

    def getMembers(self):
        return self.__members

    # Setter methods
    def setFirstName(self, firstName):
        self.__firstName = firstName

    def setLastName(self, lastName):
        self.__lastName = lastName

    def setEmail(self, email):
        self.__email = email

    def setDepartment(self, department):
        self.__department = department

    def addMember(self, employee):
        self.__members.append(employee)

    # Abstract methods
    def checkRequest(self):
        return "Request has been checked"

    def addUser(self):
        return "User has been added"

    # Public methods
    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName + " " + lastName)
        self.email = email
        self.department = department

    def checkRequest(self):
        return "Request has been checked"

    def addUser(self):
        return "User has been added"

    def login(self):
        return f"{self.email} has logged in"

    def logout(self):
        return f"{self.email} has logged out"

    # Getter methods
    def getEmail(self):
        return self.email

    def getDepartment(self):
        return self.department

    # Setter methods
    def setEmail(self, email):
        self.email = email

    def setDepartment(self, department):
        self.department = department


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.setStatus("closed")
print(req2.closeRequest())